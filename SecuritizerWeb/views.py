from django.http import HttpResponse
from django.template import Context, loader
from SecuritizerWeb.models import Password
import urllib
import json
import converter

import datetime
# Create your views here.
def index(request):
    password = Password()
    t = loader.get_template("index.html")
    c = Context({
        'password', password
    })
    return HttpResponse(t.render(c))

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)# Create your views here.

def makePasswords(request):
    r = request.GET
    quotes = converter.getQuotes(r['words'])
    passwords = converter.parse_quotes(quotes)
    jsonpasswords = json.JSONEncoder().encode(passwords)

    return HttpResponse(jsonpasswords)


