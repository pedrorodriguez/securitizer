from random import randint
import urllib
import string


def parse_quotes(qlist):
    parsed = list()
    for quote in qlist:
        parsed.append(convert(quote))
    parsed = ["".join(i for i in token.split()[:4]) for token in parsed]
    parsed = [token.replace("'", '')+str(randint(0,9)) for token in parsed]
    return parsed


#def convert(quote):
#    result = quote.lower()
#    resultStr = ""
#    replaced = False
#    for c in list(result):
#        if (replaced):
#            resultStr += c
#        else:
#            resultStr += alpha_substitute(digit_substitute(c))
#            replaced = True
#    result = upperize(resultStr)
#    return result
def convert(quote):
    result = quote.lower()
    randNum = randint(0, len(result) - 1)
    resultList = list(result)
    index = 0
    changed = False
    while (index < len(resultList)):
        if (index == randNum):
            changed = alpha_substitute(digit_substitute(resultList[index]))
            if (changed != resultList[index]):
                resultList[index] = alpha_substitute(digit_substitute(resultList[index]))
                changed = True
        index += 1
    if (not changed):
        index = 0
        while (index < len(resultList)):
            original = resultList[index]
            changed = alpha_substitute(digit_substitute(resultList[index]))
            if (original != changed):
                resultList[index] = changed
                break
            index += 1
    return upperize("".join(resultList))
def alpha_substitute(char):
    TABLE = dict([('a', '@'), ('b', '13'), 
                  ('c', '('), ('d', 'ol'),
                  ('i', '!'), ('l', '|'),
                  ('s', '$'), ('v', '\/'),
                  ('w', 'vv'), ('o', '0')]);
    if char in TABLE:
        return TABLE[char]
    else:
        return char

def digit_substitute(char):
    D_TABLE = dict([('0', 'O'), ('1', '|'), ('2', 'two'), ('3', 'iii'),
                    ('5', 'V'), ('6', 'b')])
    if char in D_TABLE:
        return D_TABLE[char]
    else:
        return char

def upperize(string):
    result = string
    for i in range(1, len(string) / 5):
        r = randint(1, len(string)) - 1
        char = result[r]
        if char.isalpha():
            char = char.upper()
        result = result[:r] + char + result[(r+1):]
    return result

def getQuotes(keywords):
    def removeNonAscii(s): return "".join(i for i in s if ord(i)<128)
    base = 'http://www.subzin.com/search.php?q='
    search_terms = keywords.replace(' ', '+')
    source = urllib.urlopen(base+search_terms).read()
    source = removeNonAscii(source).decode()
    
    lines = source.strip().split('data-content-theme="a">')[2:]
    lines = [token.replace('</a></i></b></p></div></li><li><div data-role="collapsible" data-collapsed="true" data-theme="a"','') for token in lines]
    lines = [token.split('>').pop() for token in lines]
    lines = [token.replace('\r\n', '').strip() for token in lines]
    lines = [removeNonAscii(token) for token in lines]
    lines = [token for token in lines if '<' not in token]
    lines = lines[:10]
    lines = ["".join(i +' ' if i!='l' else 'I ' for i in token.split()) for token in lines]
    lines = [token.replace("l'", "I'") for token in lines]
    return lines

#sample = ["The eagle has landed", "You shall not pass", "With great power comes great responsibilities"]
#parse_quotes(sample)
    
