/**
 * User: PedroRodriguez
 * Date: 9/29/12
 * Time: 4:37 PM
 */
$(document).ready(function() {
    $("#GeneratePassword").click(function() {
        var text = $("#Password").val();
        if (text != "") {
            $.blockUI({message : null});
            $("#Error").css("visibility", "hidden");
            $("#Loading").slideDown(500, makePasswordAjaxCall(text));
            $("#Results").empty();
        } else {
            $("#Error").css("visibility", "visible");
        }
    });
});
function makePasswordAjaxCall(pass) {
    $.ajax({
        type : "GET",
        url : "/Generate",
        data : { words : pass },
        success : displayOptions
    });
}
function displayOptions(options) {
    var optionsArray = eval(options);
    var length = optionsArray.length;
    for (i = 0; i < length; i++) {
        $("#Results").append('<div class="ResultEntry"><p>' + optionsArray[i] + '</p><a href="#" class="testPass" onclick="startTestPass(\'' + optionsArray[i] + '\')">Memory Test</a></div>');
    }
    if (length == 0) {
        $("#Results").append('<p>No Quotes Found</p>')
    }
    $("#Loading").slideUp(500, function() {
        $.unblockUI();
        $("#Results").slideDown();
    });
}
function startTestPass(pass) {
    $.blockUI({
        message : '<div id="Test"><input type="text" id="TestPassword" class="FancyTextBox"/><button class="fancyButton fancyButtonBlue" id="ClickTestPassword" onclick="testPass(\'' + pass + '\')">Memory Test</button></div>'
    });
    $('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
}
function testPass(pass) {
    if ($("#TestPassword").val() == pass) {
        $.unblockUI();
        $.growlUI('Success', 'The passwords matched');
    } else {
        $('#TestPassword').val("Wrong Password");
    }
}